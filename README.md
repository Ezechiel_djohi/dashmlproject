# Data Market Job Trends Dashboard

This repository contains a Dash application designed to visualize trends in the job market related to data science, engineer, analytics roles. The application provides insights into salaries, commonly used tools, required experiences, degrees, job types, and profiles. Additionally, it features a map visualization of job distributions across countries and a recommendation system for job seekers.

## Project Goals

The primary goals of this project are:

- To build an interactive dashboard that visualizes various aspects of the job market for data-related roles.
- To provide insights into salary trends, tools most used in the industry, required experiences, degrees, job types, and common job profiles.
- To offer a map visualization showing the distribution of data-related jobs across different countries.
- To implement a recommendation system that matches job seekers with suitable job listings based on their profiles using a K-Nearest Neighbors (KNN) model.


# New Dash App

This repository contains a Dash application.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed Python 3.11 or later.
- You have installed `pip` (Python package installer).


# Data Market Job Trends Dashboard

This repository contains a Dash application designed to visualize trends in the job market related to data science, engineer, analytics roles. The application provides insights into salaries, commonly used tools, required experiences, degrees, job types, and profiles. Additionally, it features a map visualization of job distributions across countries and a recommendation system for job seekers.

## Project Goals

The primary goals of this project are:

- To build an interactive dashboard that visualizes various aspects of the job market for data-related roles.
- To provide insights into salary trends, tools most used in the industry, required experiences, degrees, job types, and common job profiles.
- To offer a map visualization showing the distribution of data-related jobs across different countries.
- To implement a recommendation system that matches job seekers with suitable job listings based on their profiles using a K-Nearest Neighbors (KNN) model.


## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed Python 3.11 or later.
- You have installed `pip` (Python package installer).

## Installation

### Clone the repository:
    ```bash
    git clone  https://gitlab.com/Ezechiel_djohi/dashmlproject.git
    cd dashmlproject
    ```

### Create and activate a virtual environment:
    ```bash
    python -m venv env
    source env/bin/activate  # On Windows, use `env\Scripts\activate`
    ```
### Install the required packages:
    ```bash
    pip install -r requirements.txt
    ```
## Usage

1. Run the Dash application:
    ```bash
    python main_dash.py
    ```

2. Open your web browser and go to `http://127.0.0.1:8050`.

Dashboard available  https://dashmlproject.onrender.com/ 
## Models

### Job Recommendation Model

#### Overview
The job recommendation model suggests relevant job opportunities based on the user's input. The model uses natural language processing (NLP) techniques to understand the user's job description and provide personalized recommendations.

#### Training Data
The model was trained on a dataset containing job descriptions, technical skills, tools, experience levels, degrees, and job links. The dataset was preprocessed to extract these relevants features for training.

#### Techniques Used
- **TF-IDF Vectorization**: To convert text data into numerical format.
- **Machine Learning Algorithm**: A recommendation algorithm that matches the user's input with similar job descriptions in the dataset. The model use cosinus similarity.

#### Usage
The model takes a job description as input and returns a list of recommended jobs.

### Salary Prediction Model

#### Overview
The salary prediction model estimates the potential salary for a given job description. This model helps users understand the market value of their skills and experience.

#### Training Data
The model was trained on a dataset containing job descriptions and their corresponding salaries. The dataset was preprocessed to clean and extract relevant features.

#### Techniques Used
- **TF-IDF Vectorization**: To convert text data into numerical format.
- **Machine Learning Algorithm**: A Random Forest Regression model that predicts salary based on the input job description.

#### Usage
The model takes a job description as input and returns the predicted annual salary.







