import dash
from dash import dcc, html, Input, Output
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from collections import Counter
import ast
import pages
import time
import joblib
from sklearn.neighbors import NearestNeighbors
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE
import plotly.graph_objects as go
import plotly.express as px
from app import app
server = app.server

import warnings
import numpy as np
import pages.recommendation
import pages.salary_pred
import os
import inspect

script_directory = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) 

dir_projet = os.path.dirname(script_directory)

# Load the dataset
df = pd.read_csv(f'{script_directory}/Data/Dash_data/dataset_job_final_version_finale.csv',sep=';')

# Define color palette
colors = {
    'background': '#1d262f',
    'text': '#33ffe6',
    'dropdown_text': '#00008b',  # Dark blue text color for dropdown
    'dropdown_bg': '#FFFFFF',    # White background for dropdown
    'label_text': '#33ffe6',     # Fluorescent green text for label
    'plot_bg': '#242e3f',
    'grid': '#5c8cbe',
    'primary': '#2bfebe',
    'secondary': '#8784e5',
    'accent': '#8784e5'
}

# List of colors for job titles
job_colors = [
    '#636EFA', '#EF553B', '#00CC96', '#AB63FA', '#FFA15A', '#19D3F3', 
    '#FF6692', '#B6E880', '#FF97FF', '#FECB52'
]

# Functions to process technical tools and skills
def process_technical_tools(df):
    all_tools = []
    for tools in df['technical_tools'].dropna():
        tools_list = ast.literal_eval(tools)
        all_tools.extend(tools_list)
    return Counter(all_tools).most_common(10)

def process_technical_skills(df):
    all_skills = []
    for skills in df['technical_skills'].dropna():
        skills_list = ast.literal_eval(skills)
        all_skills.extend(skills_list)
    return Counter(all_skills).most_common(10)

# Function to parse job salary in Euros
def parse_job_salary(salary):
    try:
        # Check if the salary is a string representation of a list
        if isinstance(salary, str) and salary.startswith('[') and salary.endswith(']'):
            salary_list = ast.literal_eval(salary)
            if isinstance(salary_list, list) and salary_list:
                return salary_list[0]
            else:
                return None
        else:
            return float(salary) if salary else None
    except ValueError:
        return None

# Convert job_salary to numeric values using parse_job_salary function
df['parsed_salary'] = df['job_salary'].apply(parse_job_salary)

# Remove rows with NaN parsed_salary
df_filtered = df.dropna(subset=['parsed_salary'])

# # Function to calculate the overall average salary
# def calculate_average_salary(dataframe):
#     average_salary = dataframe['parsed_salary'].mean()
#     return average_salary

# # Calculate average salary
# average_salary = calculate_average_salary(df_filtered)

# Function to apply a small incremented offset to jobs with the same location
def apply_offset(df):
    grouped = df.groupby(['latitude', 'longitude']).size().reset_index(name='count')
    df_offset = df.copy()
    for idx, row in grouped.iterrows():
        if row['count'] > 1:
            indices = df[(df['latitude'] == row['latitude']) & (df['longitude'] == row['longitude'])].index
            for i, index in enumerate(indices):
                df_offset.at[index, 'latitude'] += (i * 0.0001)
                df_offset.at[index, 'longitude'] += (i * 0.0001)
    return df_offset

# Apply offset to job locations
df_offset = apply_offset(df)

# Update hover text for individual job offers
df_offset['hover_text'] = df_offset.apply(
    lambda row: f"{row['company_name']}<br>{row['job_title']}<br>{'€' + str(row['parsed_salary']) + '/year' if pd.notna(row['parsed_salary']) else ''}<br>{row['job_country']}<br><a href='{row['job_link']}' target='_blank'>Job Link</a>", axis=1
)

# Function to get the center coordinates of a country
def get_country_center(df):
    country_centers = df.groupby('job_country')[['latitude', 'longitude']].mean().reset_index()
    return country_centers

# Get the center coordinates for each country
country_centers = get_country_center(df)

# Add job count to country centers
country_job_counts = df['job_country'].value_counts().reset_index()
country_job_counts.columns = ['job_country', 'count']
country_centers = country_centers.merge(country_job_counts, on='job_country')

def create_figures(df, job_title=None):
    if job_title and job_title != 'All':
        title_df = df[df['job_title'] == job_title]
        job_title_text = job_title
    else:
        title_df = df
        job_title_text = 'All Jobs'
    
    # Job Locations Map
    df_map = title_df.dropna(subset=['latitude', 'longitude']).copy()
    df_map['parsed_salary'] = df_map['job_salary'].apply(parse_job_salary)
    
    # Calculate the number of job offers for each country
    country_counts = df_map['job_country'].value_counts().reset_index()
    country_counts.columns = ['job_country', 'job_count']
    
    # Merge the counts back to the country centers
    country_centers_counts = country_centers.merge(country_counts, on='job_country')

    # Define fixed color codes
    fixed_colors = ['#FFFF00', '#EF553B', '#00CC96', '#AB63FA', '#FFA15A', '#19D3F3', '#FF6692']
    
    # Map job titles to specific colors
    job_title_colors = {job_title: fixed_colors[i % len(fixed_colors)] for i, job_title in enumerate(df['job_title'].unique())}

    # Create the map figure
    map_fig = go.Figure()

    # Add actual job dots with fixed colors based on job titles
    map_fig.add_trace(go.Scattermapbox(
        lat=df_offset['latitude'],
        lon=df_offset['longitude'],
        mode='markers',
        marker=dict(
            size=8,
            color= df_offset['job_title'].map(job_title_colors) if job_title_text == 'All Jobs' else [job_title_colors[job_title] for i in range(len(df_offset)) ],
            opacity=0.7
        ),
        hovertext=df_offset['hover_text'],
        hoverinfo='text',
        showlegend=False  # Ensure this trace does not appear in the legend
    ))

   # Add country centers with job counts as white dots
    max_count = country_centers_counts['count'].max()
    min_size = 20
    max_size = 100
    sizeref = max_count / max_size  # Scale factor to adjust the size of the markers
    country_centers_counts['size'] = country_centers_counts['count'] / sizeref
    country_centers_counts['size'] = country_centers_counts['size'].apply(lambda x: min_size if x < min_size else x)

    map_fig.add_trace(go.Scattermapbox(
     lat=country_centers_counts['latitude'],
     lon=country_centers_counts['longitude'],
     mode='markers',
     marker=dict(
        size=country_centers_counts['size'],
        color='rgba(255, 255, 255, 0.7)',
        opacity=0.5
    ),
    hoverinfo='skip',
    name='job count by country'
))


    # Update layout with legend explaining the colors
    map_fig.update_layout(
        title='Job Locations',
        mapbox=dict(
            style='carto-darkmatter',
            center={'lat': 0, 'lon': 0},
            zoom=1.5
        ),
        paper_bgcolor=colors['plot_bg'],
        font_color=colors['text'],
        margin={'r': 0, 't': 0, 'l': 0, 'b': 0},
        legend=dict(
            title='Job Title',
            itemsizing='constant',
            font=dict(color=colors['text']),
            bgcolor=colors['plot_bg'],
        )
    )

    # Add legend for job titles
    for job_title, color in job_title_colors.items():
        map_fig.add_trace(go.Scattermapbox(
            lat=[None], lon=[None], mode='markers',
            marker=dict(size=8, color=color),
            name=job_title,
            showlegend=True
        ))

    # Job Count by Job Title (Static Plot)
    job_count = df['job_title'].value_counts().reset_index()
    job_count.columns = ['job_title', 'count']
    job_count_fig = px.bar(job_count, x='job_title', y='count', title='Job Count by Job Title', color='job_title', color_discrete_sequence=['#2bfebe', '#8784e5'])
    job_count_fig.update_layout(
        paper_bgcolor=colors['plot_bg'],
        plot_bgcolor=colors['plot_bg'],
        font_color=colors['text'],
        showlegend=False  # Remove legend
    )
    
    # Job Count by Country
    job_count_by_country = title_df['job_country'].value_counts().reset_index()
    job_count_by_country.columns = ['country', 'count']
    job_count_by_country_fig = px.bar(job_count_by_country, x='country', y='count', title=f'{job_title_text} Job Count by Country', color='country', color_discrete_sequence=['#2bfebe', '#8784e5'])
    job_count_by_country_fig.update_layout(
        paper_bgcolor=colors['plot_bg'],
        plot_bgcolor=colors['plot_bg'],
        font_color=colors['text'],
        showlegend=False  # Remove legend
    )
    
    return map_fig, job_count_fig, job_count_by_country_fig


# Prepare figures with customized styles
# Functions to create radar plots
def create_radar_figures(df, job_title):
    if job_title and job_title != 'All':
        title_df = df[df['job_title'] == job_title]
    else:
        title_df = df
    
    top_tools = process_technical_tools(title_df)
    tools, tool_counts = zip(*top_tools)
    radar_tools_fig = go.Figure()
    radar_tools_fig.add_trace(go.Scatterpolar(
        r=tool_counts,
        theta=tools,
        fill='toself',
        name=f'Top 10 Technical Tools for {job_title}'
    ))
    radar_tools_fig.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=True,
                range=[0, max(tool_counts)]
            )),
        showlegend=False,
        title=f'Top 10 Technical Tools for {job_title}',
        paper_bgcolor=colors['plot_bg'],
        font_color=colors['text']
    )
    
    top_skills = process_technical_skills(title_df)
    skills, skill_counts = zip(*top_skills)
    radar_skills_fig = go.Figure()
    radar_skills_fig.add_trace(go.Scatterpolar(
        r=skill_counts,
        theta=skills,
        fill='toself',
        name=f'Top 10 Technical Skills for {job_title}'
    ))
    radar_skills_fig.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=True,
                range=[0, max(skill_counts)]
            )),
        showlegend=False,
        title=f'Top 10 Technical Skills for {job_title}',
        paper_bgcolor=colors['plot_bg'],
        font_color=colors['text']
    )
    
    return radar_tools_fig, radar_skills_fig

# Function to plot backgrounds as horizontal bar plot with percentages for each job title
def plot_backgrounds_by_job_title(df, job_title):
    if job_title and job_title != 'All':
        title_df = df[df['job_title'] == job_title]
    else:
        title_df = df

    backgrounds_count = title_df['backgrounds'].value_counts(normalize=True).reset_index()
    backgrounds_count.columns = ['background', 'percentage']
    backgrounds_count['percentage'] *= 100  # Convert to percentage

    title = f'Percentage of Each Background{" for " + job_title if job_title and job_title != "All" else " for All Jobs"}'
    fig = px.bar(
        backgrounds_count,
        x='percentage',
        y='background',
        orientation='h',
        title=title,
        labels={'background': 'Background', 'percentage': 'Percentage (%)'},
        color_discrete_sequence=['#2bfebe', '#8784e5']
    )

    fig.update_layout(
        xaxis_title='Percentage (%)',
        yaxis_title='Background',
        showlegend=False,  # Remove legend
        paper_bgcolor=colors['plot_bg'],
        plot_bgcolor=colors['plot_bg'],
        font_color=colors['text']
    )
    
    return fig

# Function to create average job salary by job title plot
def plot_average_salary_by_title(dataframe):
    average_salary_by_title = dataframe.groupby('job_title')['parsed_salary'].mean().reset_index()
    avg_salary_fig = px.bar(
        average_salary_by_title,
        x='job_title',
        y='parsed_salary',
        title='Average Salary by Job Title (in Euros)',
        color='job_title',
        color_discrete_sequence=['#2bfebe', '#8784e5']
    )
    avg_salary_fig.update_layout(
        paper_bgcolor=colors['plot_bg'],
        plot_bgcolor=colors['plot_bg'],
        font_color=colors['text'],
        yaxis_title='Average Salary (€)',
        xaxis_title='Job Title',
        showlegend=False  # Remove legend
    )
    return avg_salary_fig

# Function to plot the distribution of job salaries
from scipy.stats import gaussian_kde

def plot_salary_distribution(dataframe, job_title):
    if job_title == 'All':
        filtered_data = dataframe
    else:
        filtered_data = dataframe[dataframe['job_title'] == job_title]
    
    # Remove outliers using IQR method
    Q1 = filtered_data['parsed_salary'].quantile(0.25)
    Q3 = filtered_data['parsed_salary'].quantile(0.75)
    IQR = Q3 - Q1
    filtered_data = filtered_data[(filtered_data['parsed_salary'] >= Q1 - 1.5 * IQR) & (filtered_data['parsed_salary'] <= Q3 + 1.5 * IQR)]
    
    # Compute density curve
    density = gaussian_kde(filtered_data['parsed_salary'])
    x = np.linspace(filtered_data['parsed_salary'].min(), filtered_data['parsed_salary'].max(), 1000)
    y = density(x)
    
    # Plot distribution of job salaries by job title as a histogram using Plotly
    fig = go.Figure()

    fig.add_trace(
        go.Histogram(
            x=filtered_data['parsed_salary'],
            nbinsx=50,
            name='Histogram',
            marker=dict(color='#2bfebe'),
            opacity=0.75
        )
    )

    fig.add_trace(
        go.Scatter(
            x=x,
            y=y,
            mode='lines',
            name='Density Curve',
            line=dict(color='#8784e5', width=2)
        )
    )

    fig.update_layout(
        title=f'Distribution of Job Salaries for {job_title}',
        paper_bgcolor=colors['plot_bg'],
        plot_bgcolor=colors['plot_bg'],
        font_color=colors['text'],
        yaxis_title='Frequency',
        xaxis_title='Job Salary in Euros',
        showlegend=False  # Remove legend
    )

    return fig

# Create initial figures
map_fig, job_count_fig, job_count_by_country_fig = create_figures(df_offset)
radar_tools_fig, radar_skills_fig = create_radar_figures(df_offset, 'All')
backgrounds_fig = plot_backgrounds_by_job_title(df_offset, 'All')
avg_salary_by_job_title_fig = plot_average_salary_by_title(df_filtered)
salary_distribution_fig = plot_salary_distribution(df_filtered, 'All')


app.config.suppress_callback_exceptions = True

# App layout
app.layout = html.Div([
    dbc.Container([
        dbc.Row([
            dbc.Col(html.Img(src="/assets/logo.png", height="200px"), width="auto"),
            dbc.Col(
                dcc.Tabs(
                    id="tabs",
                    value='tab-1',
                    children=[
                        dcc.Tab(label='Data Science Job Market Analysis', value='tab-1', className='custom-tab', selected_className='custom-tab--selected'),
                        dcc.Tab(label='Looking for a Job?', value='tab-2', className='custom-tab', selected_className='custom-tab--selected'),
                        dcc.Tab(label='Salary Estimation', value='tab-3', className='custom-tab', selected_className='custom-tab--selected'),
                    ],
                    className='custom-tabs'
                ),
                width=True
            ),
        ], align="center", className="flex-nowrap g-0"),
    ], fluid=True, className="my-4"),
    dbc.Container([
        html.Div(id='tabs-content')
    ], fluid=True)
])

@app.callback(Output('tabs-content', 'children'),
              [Input('tabs', 'value')])
def render_content(tab):
    if tab == 'tab-1':
        return html.Div([
            html.Div(className="quick-stats", children=[
                html.Div(className="quick-stat-box", children=[
                    html.H2(id="total-jobs", style={"font-size": "3em"}),
                    html.H4("Total Jobs", style={"font-size": "2em"})
                ]),
                html.Div(className="quick-stat-box", children=[
                    html.H2(id="average-salary", style={"font-size": "3em"}),
                    html.H4("Average Salary", style={"font-size": "2em"})
                ]),
                html.Div(id="top-hiring-companies1", className="quick-stat-box"),
                html.Div(id="top-hiring-companies2", className="quick-stat-box"),
                html.Div(id="top-hiring-companies3", className="quick-stat-box")
            ]),

            html.Label("Select a Job Title", style={'color': colors['label_text'], 'font-size': '1.5em'}),
            dcc.Dropdown(
                id='job-title-dropdown',
                options=[{'label': 'All', 'value': 'All'}] + [{'label': title, 'value': title} for title in df['job_title'].unique()],
                value='All',
                clearable=False,
                style={'color': colors['dropdown_text'], 'background-color': colors['background'], 'font-size': '1.2em'}
            ),
            dbc.Row([
                dbc.Col(dcc.Graph(id='map-fig', className="graph-container", figure=map_fig), width=12)
            ], className="mb-4"),
            dbc.Row([
                dbc.Col(dcc.Graph(id='job-count-fig', figure=job_count_fig, className="graph-container"), width=4),
                dbc.Col(dcc.Graph(id='avg-salary-fig', figure=avg_salary_by_job_title_fig, className="graph-container"), width=4),
                dbc.Col(dcc.Graph(id='job-count-by-country-fig', figure=job_count_by_country_fig, className="graph-container"), width=4)
            ]),
            dbc.Row([
                dbc.Col(dcc.Graph(id='backgrounds-fig', figure=backgrounds_fig, className="graph-container"), width=4),
                dbc.Col(dcc.Graph(id='salary-distribution', figure=salary_distribution_fig, className="graph-container"), width=4),
                dbc.Col(dcc.Graph(id='experience-level-distribution', className="graph-container"), width=4)
            ]),
            dbc.Row([
                dbc.Col(dcc.Graph(id='degree-requirements', className="graph-container"), width=4),
                dbc.Col(dcc.Graph(id='radar-tools-fig', figure=radar_tools_fig, className="graph-container"), width=4),
                dbc.Col(dcc.Graph(id='radar-skills-fig', figure=radar_skills_fig, className="graph-container"), width=4)
            ])
        ])
    elif tab == 'tab-2':
        return html.Div([
            pages.recommendation.layout,
            html.Div(id='job-search-content')
        ])
    elif tab == 'tab-3':
        return html.Div([
            pages.salary_pred.layout,
            html.Div(id='salary-pred-content')
        ])

@app.callback(
    [Output('average-salary', 'children'),
     Output('total-jobs', 'children'),
     Output('top-hiring-companies1', 'children'),
     Output('top-hiring-companies2', 'children'),
     Output('top-hiring-companies3', 'children'),
     Output('map-fig', 'figure'),
     Output('job-count-fig', 'figure'),
     Output('avg-salary-fig', 'figure'),
     Output('job-count-by-country-fig', 'figure'),
     Output('backgrounds-fig', 'figure'),
     Output('salary-distribution', 'figure'),
     Output('experience-level-distribution', 'figure'),
     Output('degree-requirements', 'figure'),
     Output('radar-tools-fig', 'figure'),
     Output('radar-skills-fig', 'figure')],
    [Input('job-title-dropdown', 'value')]
)
def update_figures(selected_title):
    map_fig, job_count_fig, job_count_by_country_fig = create_figures(df_offset, selected_title)
    radar_tools_fig, radar_skills_fig = create_radar_figures(df_offset, selected_title)
    backgrounds_fig = plot_backgrounds_by_job_title(df_offset, selected_title)
    average_salary, total_jobs, top_hiring_divs = update_kpi(selected_title)
    # Experience Level Distribution
    title_df = df if selected_title == 'All' else df[df['job_title'] == selected_title]
    experience_level_count = title_df['experience_levels'].value_counts().reset_index()
    experience_level_count.columns = ['experience_level', 'count']
    experience_level_distribution_fig = px.bar(experience_level_count, x='experience_level', y='count', title=f'Experience Level Requirements for {selected_title}', color='experience_level', color_discrete_sequence=['#2bfebe', '#8784e5'])
    experience_level_distribution_fig.update_layout(
        paper_bgcolor=colors['plot_bg'],
        plot_bgcolor=colors['plot_bg'],
        font_color=colors['text'],
        showlegend=False  # Remove legend
    )

    # Degree Requirements
    degree_count = title_df['degree_required'].value_counts().reset_index()
    degree_count.columns = ['degree_required', 'count']
    degree_requirements_fig = px.bar(degree_count, x='degree_required', y='count', title=f'Degree Requirements for {selected_title}', color='degree_required', color_discrete_sequence=['#2bfebe', '#8784e5'])
    degree_requirements_fig.update_layout(
        paper_bgcolor=colors['plot_bg'],
        plot_bgcolor=colors['plot_bg'],
        font_color=colors['text'],
        showlegend=False  # Remove legend
    )

    # Salary Distribution
    salary_distribution_fig = plot_salary_distribution(df_filtered, selected_title)
    
    # Average Salary by Job Title 
    avg_salary_by_job_title_fig = plot_average_salary_by_title(df_filtered)
    
    return ( f"{average_salary:,.2f} €", f"{total_jobs:,}", top_hiring_divs[0],top_hiring_divs[1],top_hiring_divs[2] , map_fig, job_count_fig, avg_salary_by_job_title_fig, job_count_by_country_fig, 
            backgrounds_fig, salary_distribution_fig, experience_level_distribution_fig, degree_requirements_fig, 
            radar_tools_fig, radar_skills_fig)



# Function to calculate the overall average salary
def update_kpi(selected_title):
    if selected_title == 'All':
        filtered_df = df_offset
    else:
        filtered_df = df_offset[df_offset['job_title'] == selected_title]
    
    average_salary = filtered_df['parsed_salary'].mean()
    total_jobs = len(filtered_df)
    
    top_hiring_companies = filtered_df['company_name'].value_counts().head(3)
    medals = ["🥇", "🥈", "🥉"]
    top_hiring_divs = [
        html.Div(children=[
            html.H2(f"{medals[i]} {company}", style={"font-size": "2.5em"}),
            html.H4(f"{count} Jobs", style={"font-size": "1.5em"})
        ]) for i, (company, count) in enumerate(top_hiring_companies.items())
    ]
    return average_salary, total_jobs, top_hiring_divs
    
    #return f"{average_salary:,.2f} €", f"{total_jobs:,}", top_hiring_divs[0],top_hiring_divs[1],top_hiring_divs[2]




if __name__ == '__main__':
    app.run_server(debug=True)