import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import pandas as pd
from app import app
import joblib
import warnings
from sklearn.feature_extraction.text import TfidfVectorizer
import sys
import os
import inspect

sys.path.append('../')
script_directory = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) 
dir_projet = os.path.dirname(script_directory)

from Fonction_utiles import data_terminology

# Charger le modèle de prédiction de salaire
model = joblib.load('Model/SalaryPred/salary_prediction_model.pkl')
vectorizer = joblib.load('Model/SalaryPred/tfidf_vectorizer.pkl')

# Define color palette
colors = {
    'background': '#1d262f',
    'text': '#33ffe6',
    'dropdown_text': '#00008b',  # Dark blue text color for dropdown
    'dropdown_bg': '#e3e6e9',    # White background for dropdown
    'label_text': '#5C8CBE',     # blue text for label
    'plot_bg': '#242e3f',   
    'title_text': '#33ffe6',     # Fluorescent green text for label
    'grid': '#5c8cbe',
    'primary': '#2bfebe',
    'secondary': '#8784e5',
    'accent': '#ff26d'
}

layout = html.Div([ 
    dbc.Container([
        dbc.Row([
            dbc.Col(html.H1("Salary Prediction", style={'color': colors['title_text'], 'font-size': '1.5em'}), className="text-center"),
        ]),
        dbc.Row([
            dbc.Col(html.Div([
                dcc.Textarea(
                    id='textarea-2',
                    placeholder='Enter long text here...',
                    style={'width': '100%', 'height': '200px','color': colors['dropdown_bg'], 'background-color': colors['background']},
                    
                )
            ], style={'margin-bottom': '20px'}))
        ]),
        dbc.Row([
            dbc.Col(html.Button('Predicted Salary', id='predict-button2', n_clicks=0, className="btn btn-primary"), width="auto"),
            dbc.Col(html.Button('Reset', id='reset-button2', n_clicks=0, className="btn btn-secondary"), width="auto")
        ]),
         html.Div(className="quick-stat-box", children=[
                    # html.H4("Estimated Salary", style={"font-size": "2em"}),
                    html.H2(id="output-prediction", style={"font-size": "2em"})
                ]),
    ], className="custom-container")
])

@app.callback(
    [Output('output-prediction', 'children'),
     Output('textarea-2', 'value'),
     Output('predict-button2', 'className'),
     Output('reset-button2', 'className')],
    [Input('predict-button2', 'n_clicks'),
     Input('reset-button2', 'n_clicks')],
    [State('textarea-2', 'value')]
)
def predict_and_reset(predict_n_clicks, reset_n_clicks, job_description):
    ctx = dash.callback_context
    if not ctx.triggered:
        return "", "", "btn btn-primary", "btn btn-secondary"
    
    button_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if button_id == 'predict-button2' and job_description:
        # Transformer la description de l'offre avec le vecteur TF-IDF
        description_tfidf = vectorizer.transform([job_description])
        
        # Prédire le salaire
        predicted_salary = model.predict(description_tfidf)
        predicted_salary_text = f"Estimated salary is: ${predicted_salary[0]:,.2f} per year"
        
        return predicted_salary_text, job_description, "btn btn-success", "btn btn-secondary"
    
    if button_id == 'reset-button2':
        return "", "", "btn btn-primary", "btn btn-danger"

    return "", "", "btn btn-primary", "btn btn-secondary"
