import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import pandas as pd
from app import app
import joblib
import warnings
from sklearn.feature_extraction.text import TfidfVectorizer
import sys
import os
import inspect

sys.path.append('../')
script_directory = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) 
dir_projet = os.path.dirname(script_directory)

from Fonction_utiles import data_terminology
from Model.JobRecommend.recommandation import JobRecommender
from Model.JobRecommend import recommandation
warnings.filterwarnings('ignore')

# Charger le modèle de recommandation de job
recommender = recommandation.JobRecommender()
recommender.load_model("Model/JobRecommend/Model/job_recommender_model.pkl")

# Define color palette
colors = {
    'background': '#1d262f',
    'text': '#33ffe6',
    'dropdown_text': '#00008b',  # Dark blue text color for dropdown
    'dropdown_bg': '#e3e6e9',    # White background for dropdown
    'label_text': '#5C8CBE',     # blue text for label
    'plot_bg': '#242e3f',   
    'title_text': '#33ffe6',     # Fluorescent green text for label
    'grid': '#5c8cbe',
    'primary': '#2bfebe',
    'secondary': '#8784e5',
    'accent': '#ff26d'
}


layout = html.Div([ 
    dbc.Container([
        dbc.Row([
            dbc.Col(html.H1("Job Recommendation",style={'color': colors['title_text'], 'font-size': '1.5em'}), className="text-center"),
        ]),
        dbc.Row([
            dbc.Col(html.Div([
                dcc.Textarea(
                    id='textarea-1',
                    placeholder="""I am seeking a challenging position as a Senior Data Scientist in New York, where I can leverage my extensive background in computer science and statistics. With a Ph.D. in Data Science and over 7 years of experience in the field, I have developed expertise in machine learning, data mining, and predictive analytics. I am proficient in technical tools such as Python, R, SQL, and Apache Spark, and have hands-on experience with data visualization tools like Tableau and Power BI. My technical skills include deep learning, natural language processing, and big data analytics. I have a proven track record of leading data science projects and collaborating with cross-functional teams to drive business insights and innovation. Additionally, I have a strong foundation in statistical modeling, mathematical optimization, and cloud computing services like AWS and Google Cloud. I am looking for a dynamic environment where I can continue to grow and contribute to cutting-edge data solutions.""",
                    style={'width': '100%', 'height': '200px','color': colors['dropdown_bg'], 'background-color': colors['background']},
                    
                )
            ], style={'margin-bottom': '20px'}))
        ]),
        dbc.Row([
            dbc.Col(html.Button('Recommend Job', id='predict-button', n_clicks=0, className="btn btn-primary"), width="auto"),
            dbc.Col(html.Button('Reset', id='reset-button', n_clicks=0, className="btn btn-secondary"), width="auto")
        ]),
        dbc.Row([
            dbc.Col(html.Div(id='output-recommendation', style={'margin-top': 20,'color': colors['dropdown_bg']}))
        ])
    ], className="custom-container")
])

@app.callback(
    [Output('output-recommendation', 'children'),
     Output('textarea-1', 'value'),
     Output('predict-button', 'className'),
     Output('reset-button', 'className')],
    [Input('predict-button', 'n_clicks'),
     Input('reset-button', 'n_clicks')],
    [State('textarea-1', 'value')]
)
def predict_and_reset(predict_n_clicks, reset_n_clicks, job_description):
    ctx = dash.callback_context
    if not ctx.triggered:
        return "", "", "", "btn btn-primary", "btn btn-secondary"
    
    button_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if button_id == 'predict-button' and job_description:
        
        # Obtenir les recommandations
        recommendations = recommender.recommend_jobs(job_description)
        recommendations_df = pd.DataFrame(recommendations)

        # Convertir le DataFrame en table HTML avec des liens cliquables
        recommendations_table = html.Table([
            html.Thead(
                html.Tr([html.Th(col) for col in recommendations_df.columns])
            ),
            html.Tbody([
                html.Tr([
                    html.Td(html.A(href=cell, children=cell, target="_blank")) if col == 'job_link' else html.Td(cell) 
                    for col, cell in row.items()
                ]) for index, row in recommendations_df.iterrows()
            ])
        ], className="table table-dark table-striped table-bordered table-hover")
        
        return recommendations_table, job_description, "btn btn-success", "btn btn-secondary"
    
    if button_id == 'reset-button':
        return "", "", "", "btn btn-primary", "btn btn-danger"

    return "", "", "", "btn btn-primary", "btn btn-secondary"