import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import pickle

class JobRecommender:
    def __init__(self):
        self.tfidf_vectorizer = TfidfVectorizer(stop_words='english')
        self.job_matrix = None
        self.job_job_df = None

    def train(self, job_df):
        self.job_job_df = job_df
        job_descriptions = job_df['job_description'].tolist()
        self.job_matrix = self.tfidf_vectorizer.fit_transform(job_descriptions)
    
    def save_model(self, model_path):
        with open(model_path, 'wb') as f:
            pickle.dump({
                'tfidf_vectorizer': self.tfidf_vectorizer,
                'job_matrix': self.job_matrix,
                'job_job_df': self.job_job_df
            }, f)
    
    def load_model(self, model_path):
        with open(model_path, 'rb') as f:
            model_job_df = pickle.load(f)
            self.tfidf_vectorizer = model_job_df['tfidf_vectorizer']
            self.job_matrix = model_job_df['job_matrix']
            self.job_job_df = model_job_df['job_job_df']
    
    def recommend_jobs(self, user_profile, top_n=5):
        user_profile_vector = self.tfidf_vectorizer.transform([user_profile])
        cosine_similarities = cosine_similarity(user_profile_vector, self.job_matrix).flatten()
        top_job_indices = cosine_similarities.argsort()[-top_n:][::-1]
        top_jobs = self.job_job_df.iloc[top_job_indices]
        top_jobs['similarity_score'] = cosine_similarities[top_job_indices]
        return top_jobs[['job_title', 'company_name', 'company_location', 'job_link', 'similarity_score']]

# Exemple d'utilisation
if __name__ == "__main__":
    # Charger les données
    job_df = dataset_job_final
    
    job_df["job_description"] = job_df['job_title'] + " " + job_df["salair_typejob"] + " " + job_df['company_name'] +" " +  job_df['company_name'] + \
         " " + job_df['company_location'] + " " + job_df['experience_levels'] + \
             " " + job_df['degree_required'] + " " + job_df['technical_tools'] + \
                 " " + job_df['technical_skills'] +" " +  job_df['backgrounds'] 
                 
    job_df = job_df[['job_title', 'job_link',
           'company_name', 'company_location', 
           'salair_typejob', 'job_description']]
    job_df = job_df.dropna(subset=['job_description'])  # Supprimer les lignes avec des NaN dans 'description_offre'


    # Entraîner le modèle
    recommender = JobRecommender()
    recommender.train(job_df)
    recommender.save_model("job_recommender_model.pkl")

    
