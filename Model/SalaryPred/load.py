

import joblib

# Chargement du modèle
model = joblib.load('salary_prediction_model.pkl')
vectorizer = joblib.load('tfidf_vectorizer.pkl')

# Exemple de prédiction pour une nouvelle description
new_description = ["Experienced software Venezuela engineer python java communication needed for exciting new project"]
new_description_tfidf = vectorizer.transform(new_description)
predicted_salary = model.predict(new_description_tfidf)
print(f'Predicted salary: ${predicted_salary}')
